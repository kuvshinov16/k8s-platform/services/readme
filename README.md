Набор сервисов 'microservices-demo' https://github.com/GoogleCloudPlatform/microservices-demo/tree/main

http://shop.158.160.146.35.nip.io/


![Alt text](image-3.png)



Реализован CI/CD со сборкой проекта, дальнейшим пушем образа в гитлаб-регистри(развернул нексус, но пока не успел все настроить) и деплоем в кластер кубера с помощью helm.

общий вид репозиториев с сервисами:

- src
исходники с докерфайлом

- helm-chart
параметризированный чарт. впроцессе деплоя подставляется образ сервиса, собранный на предыдущем шаге (build)

![Alt text](image.png)

- build

![Alt text](image-1.png)

- deploy

![Alt text](image-2.png)


